import copy
import pickle
import numpy as np
import collections
from sklearn.tree import DecisionTreeRegressor
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error, accuracy_score
from scipy.special import logsumexp


class GradientBoostingClassifier:
    def __init__(self, min_leaf_size=1, max_depth=1, n_estimators=10, max_leaf_nodes=8, lr=0.01):
        self.min_leaf_size = min_leaf_size
        self.max_depth = max_depth
        self.n_estimators = n_estimators
        self.max_leaf_nodes = max_leaf_nodes
        self.lr = lr
        self.trees = []
        self.loss = []
        self.accuracy = []
        self.val_loss = []
        self.val_accuracy = []

    def __to_categorical(self, x):
        encoder = LabelEncoder()
        lx = encoder.fit_transform(x)
        self.labels_encoder = encoder
        self.classes = collections.OrderedDict(sorted(dict(zip(lx, encoder.inverse_transform(lx))).items()))
        return lx

    @staticmethod
    def __residuals(true, pred):
        return true - pred

    @staticmethod
    def __loss(true, pred):
        return .5 * (true - pred) ** 2

    def fit(self, x_train_orig, y_train_orig, val_part=0.0):
        if val_part:
            x_train_orig, x_val_orig, y_train_orig, y_val_orig = train_test_split(x_train_orig, y_train_orig,
                                                                                  shuffle=True,
                                                                                  test_size=val_part,
                                                                                  random_state=555)

        x_train, y_train = copy.deepcopy(x_train_orig), copy.deepcopy(y_train_orig)

        # categorize class labels
        y_train = self.__to_categorical(y_train)
        y_train = np.array([
            [0 if class_idx != class_idx_train else 1 for class_idx in self.classes] for class_idx_train in y_train
        ])
        if val_part:
            y_val = np.array([
                [0 if class_idx != class_idx_val else 1 for class_idx in self.classes]
                for class_idx_val in self.labels_encoder.fit_transform(y_val_orig)
            ])

        # init "base" predictions
        y_pred = np.full((x_train.shape[0], len(self.classes)), .0)
        for i in range(self.n_estimators):
            # fit weak learner to residuals
            tree = DecisionTreeRegressor(min_samples_leaf=self.min_leaf_size,
                                         max_depth=self.max_depth,
                                         max_leaf_nodes=self.max_leaf_nodes)
            residuals = y_train - y_pred
            tree.fit(x_train, residuals)

            # calculate step length
            new_predicted = tree.predict(x_train)

            # update prediction
            y_pred += self.lr * new_predicted

            # update ensemble
            self.trees.append(tree)
            # update loss and accuracy logs
            y_pred_train = self.predict(x_train)
            y_pred_proba_train = self.predict_proba(x_train)
            self.accuracy.append(accuracy_score(y_train_orig, y_pred_train))
            self.loss.append(mean_squared_error(y_train, y_pred_proba_train))
            if val_part:
                y_pred_val = self.predict(x_val_orig)
                y_pred_proba_val = self.predict_proba(x_val_orig)
                self.val_accuracy.append(accuracy_score(y_val_orig, y_pred_val))
                self.val_loss.append(mean_squared_error(y_val, y_pred_proba_val))

    def predict(self, x):
        y_pred = np.full((x.shape[0], len(self.classes)), .0)
        for tree in self.trees:
            new_predict = tree.predict(x)
            y_pred += self.lr * new_predict

        probs = np.nan_to_num(np.exp(y_pred - (logsumexp(y_pred, axis=1)[:, np.newaxis])))
        probs = np.argmax(probs, axis=1)
        return self.labels_encoder.inverse_transform(probs)

    def predict_proba(self, x):
        y_pred = np.full((x.shape[0], len(self.classes)), .0)
        for tree in self.trees:
            new_predict = tree.predict(x)
            y_pred += self.lr * new_predict

        probs = np.nan_to_num(np.exp(y_pred - (logsumexp(y_pred, axis=1)[:, np.newaxis])))
        return probs

    def save(self, filepath=None):
        with open(filepath or '' + 'gbc.pkl', 'wb') as f:
            pickle.dump(self, f)
