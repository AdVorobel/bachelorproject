import * as React from 'react';
import FormControl from '@mui/material/FormControl';
import Button from '@mui/material/Button';
import Autocomplete from "@material-ui/lab/Autocomplete";
import TextField from "@material-ui/core/TextField";

const SELECT_WIDTH = 1000;
const PREDICTION_TEXT = "Given symptoms indicate a probable "

const requestOptions = {
    headers: {
       'Content-Type': 'application/json',
       "Access-Control-Allow-Origin": "*",
       "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept"
    }
};

export default function MultipleSelectChip() {
  const [symptomNames, setSymptomNames] = React.useState([]);
  const [data, setData] = React.useState([]);
  const [prediction, setPrediction] = React.useState([]);

  React.useEffect(() => {
    const req = {
        'method': 'GET',
        ...requestOptions
     }
    fetch('http://localhost:5000/api/symptoms', req)
            .then(response => response.json())
            .then(data => setData(data.symptomsList))
  }, []);

  const handleSelectChange = (event, value) => {
    console.log(value)
    setSymptomNames(value);
    setPrediction('')
  };

  const OnBtnClick = () => {
     const req = {
        'method': 'POST',
        'body': JSON.stringify({"symptoms": symptomNames}),
        ...requestOptions
     }
     fetch('http://localhost:5000/api/check-me', req)
            .then(response => response.json())
            .then(data => {
                const text = PREDICTION_TEXT + `"${data.prediction}"`
                setPrediction(text)
            })
            .catch(error => console.log(error))
  }

  return (
    <div className="size-inherit content">
      <h1 className="prediction-text">{prediction}</h1>
      <FormControl>
        <Autocomplete
          multiple
          options={data}
          getOptionLabel={(option) => option.text}
          style={{ width: SELECT_WIDTH }}
          onChange={handleSelectChange}
          renderInput={(params) => (
             <TextField {...params} label="Symptoms" variant="outlined" />
          )}
        />
      </FormControl>
      <Button sx={{ m: 3, width: SELECT_WIDTH * 0.25 }} variant="contained" onClick={OnBtnClick}>Check me</Button>
    </div>
  );
}
