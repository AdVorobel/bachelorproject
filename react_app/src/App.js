import * as React from 'react';

import './App.css';
import CustomSelect from './components/CustomSelect.jsx';

function App() {
  return (
    <div className="App">
        <div className="size-inherit content">
            <CustomSelect/>
        </div>
    </div>
  );
}

export default App;
